package com.AfricanBeauty_AFRICA.Memory;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePref {

    private SharedPreferences sharedPreferences;

    public SharePref (Context context){
        sharedPreferences = context.getSharedPreferences("KEY", Context.MODE_PRIVATE);
    }

    public void putmac_address(String Key, String Value){
        SharedPreferences.Editor editor =sharedPreferences.edit();
        editor.putString(Key, Value);
        editor.apply();
    }

    public String getmac_addresss(String Key){
        return sharedPreferences.getString(Key, null);
    }
}
