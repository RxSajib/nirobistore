package com.AfricanBeauty_AFRICA.SplashScreen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.AfricanBeauty_AFRICA.store.DataManager;
import com.AfricanBeauty_AFRICA.store.MainActivity;
import com.AfricanBeauty_AFRICA.store.R;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LandingScreen extends AppCompatActivity {

    private RelativeLayout WigsButton, CreamButton, ClothsButton, FoodButton, MoreButton;
    private RelativeLayout Whatsappbutton;
    private MaterialTextView WighCountText, CreamCountText, ClothsCountText, FoodCountText, OthersCountText;
    private DatabaseReference MCategoryDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);


        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        init_view();
        getcount();

    }

    private void init_view(){

        WighCountText = findViewById(R.id.WigsCountTextID);
        CreamCountText = findViewById(R.id.CreamCountID);
        ClothsCountText = findViewById(R.id.ClothsCountTextID);
        FoodCountText = findViewById(R.id.FoodCountTextID);
        OthersCountText = findViewById(R.id.OthersCountTextID);

        Whatsappbutton = findViewById(R.id.WhatsappButtonID);
        Whatsappbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://api.whatsapp.com/send?phone="+DataManager.WhatsappNumber;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        WigsButton = findViewById(R.id.WigsButtonID);
        CreamButton = findViewById(R.id.CreamButtonID);
        ClothsButton = findViewById(R.id.ClothsButonID);
        FoodButton =  findViewById(R.id.FoodButtonID);
        MoreButton = findViewById(R.id.MoreButtonID);

        WigsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(DataManager.Category, DataManager.Wigs);
                startActivity(intent);
                Animatoo.animateSlideLeft(LandingScreen.this);

            }
        });

        CreamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(DataManager.Category, DataManager.Creams);
                startActivity(intent);
                Animatoo.animateSlideLeft(LandingScreen.this);
            }
        });

        ClothsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(DataManager.Category, DataManager.Cloths);
                startActivity(intent);
                Animatoo.animateSlideLeft(LandingScreen.this);
            }
        });

        FoodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(DataManager.Category, DataManager.Food);
                startActivity(intent);
                Animatoo.animateSlideLeft(LandingScreen.this);
            }
        });

        MoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(DataManager.Category, DataManager.Others);
                startActivity(intent);
                Animatoo.animateSlideLeft(LandingScreen.this);
            }
        });
    }


    private void getcount(){

        MCategoryDatabase = FirebaseDatabase.getInstance().getReference().child(DataManager.ProductRef);


        MCategoryDatabase.child(DataManager.Wigs)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        if(snapshot.exists()){
                            int count = (int)snapshot.getChildrenCount();
                            WighCountText.setText("Total "+String.valueOf(count)+" Item");

                            Log.d("Count", String.valueOf(count));
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

        MCategoryDatabase.child(DataManager.Creams)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            int count =(int) snapshot.getChildrenCount();
                            CreamCountText.setText("Total "+String.valueOf(count)+" Item");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

        MCategoryDatabase.child(DataManager.Cloths).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    int count = (int) snapshot.getChildrenCount();
                    ClothsCountText.setText("Total "+String.valueOf(count)+" Item");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        MCategoryDatabase.child("Food")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            int count = (int) snapshot.getChildrenCount();
                            FoodCountText.setText("Total "+String.valueOf(count)+" Item");

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

        MCategoryDatabase.child(DataManager.Others).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    int count = (int) snapshot.getChildrenCount();
                    OthersCountText.setText("Total "+String.valueOf(count)+" Item");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}