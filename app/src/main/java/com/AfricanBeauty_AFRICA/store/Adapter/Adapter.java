package com.AfricanBeauty_AFRICA.store.Adapter;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.AfricanBeauty_AFRICA.store.DataManager;
import com.AfricanBeauty_AFRICA.store.Model.CategoryModel;
import com.AfricanBeauty_AFRICA.store.R;
import com.AfricanBeauty_AFRICA.store.ViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Adapter extends FirebaseRecyclerAdapter<CategoryModel, ViewHolder> {

    private OnClick OnClick;
    private getData getData;

    public Adapter(@NonNull FirebaseRecyclerOptions<CategoryModel> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull CategoryModel model) {
        Picasso.with(holder.imageView.getContext()).load(model.getImageOneLink()).networkPolicy(NetworkPolicy.OFFLINE).into(holder.imageView
                , new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
        Picasso.with(holder.imageView.getContext()).load(model.getImageOneLink()).into(holder.imageView);
                    }
                });

        holder.ProductDetails.setText(model.getDetails());
        holder.ProductCondition.setText(model.getType());
        holder.ProductPrice.setText("QTR "+model.getPrice());
        holder.ProductName.setText(model.getTitle());


        String Timestamp = model.getTimestamp();
        long Timestamplong = Long.parseLong(Timestamp);


        Calendar calendar_timestamp_time = Calendar.getInstance(Locale.ENGLISH);
        calendar_timestamp_time.setTimeInMillis(Timestamplong * 1000);
        String TimestampTime = DateFormat.format(DataManager.TimePattern, calendar_timestamp_time).toString();


        Calendar calendar_timestamp_date = Calendar.getInstance(Locale.ENGLISH);
        calendar_timestamp_date.setTimeInMillis(Timestamplong * 1000);
        String TimestampDate = DateFormat.format(DataManager.DatePattern, calendar_timestamp_date).toString();



        Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
        SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimePattern);
        String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

        Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
        SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DatePattern);
        String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());


        if(TimestampDate.equals(CurrentDate)){
            holder.Time.setText("Today");
        }
        else {
            holder.Time.setText(TimestampDate);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClick.Click(getRef(position).getKey(), model.getCategory());
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_layout, parent, false);
        return new ViewHolder(view);
    }


    public interface OnClick{
        void Click(String UID, String Category);
    }

    public void setOnclickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }


    @Override
    public void onDataChanged() {
        getData.Dataget();
        super.onDataChanged();

    }

    public interface getData{
        void Dataget();
    }

    public void setDataSuccessLisiner(getData getData){
        this.getData = getData;
    }
}
