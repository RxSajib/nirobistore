package com.AfricanBeauty_AFRICA.store.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.AfricanBeauty_AFRICA.store.Model.BannerModel;
import com.AfricanBeauty_AFRICA.store.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    private Context context;
    private List<BannerModel> bannerModels;

    public ViewPagerAdapter(Context context, List<BannerModel> bannerModels) {
        this.context = context;
        this.bannerModels = bannerModels;
    }

    @Override
    public int getCount() {
        return bannerModels.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view ==object;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((View) object);
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        View Mview = LayoutInflater.from(context).inflate(R.layout.viewpager_layout, container, false);

        ImageView imageView = Mview.findViewById(R.id.ViewPagerImage);


        Picasso.with(context).load(bannerModels.get(position).getImage()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.loding)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
        Picasso.with(context).load(bannerModels.get(position).getImage()).into(imageView);
                    }
                });


        container.addView(Mview);
        return Mview;
    }
}
