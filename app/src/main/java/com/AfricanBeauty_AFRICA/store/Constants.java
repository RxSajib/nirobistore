package com.AfricanBeauty_AFRICA.store;

public class  Constants {

     public static final String Package_Name = "com.Nairobi.store";
     public static final String RESULT_DATA_KEY = Package_Name+".RESULT_DATA_KEY";
     public static final String RECEIVER = Package_Name+".RECEIVER";
     public static final String LOCATION_DATA_EXTRA = Package_Name+".LOCATION_DATA_EXTRA";
     public static final int SUCCESS_RESULT = 1;
     public static final int FAILURE_RESULT = 0;
}
