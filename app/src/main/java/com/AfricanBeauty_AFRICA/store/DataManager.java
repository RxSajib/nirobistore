package com.AfricanBeauty_AFRICA.store;

public class DataManager {

    public static final String ProductRef = "Product";

    public static final String ProductDetails = "Details";
    public static final String ProductImageOne = "ImageOneLink";
    public static final String ProductImageTwo = "ImageTwoLink";
    public static final String ProductImageThree =  "ImageThreeLink";
    public static final String ProductImageFour = "ImageFourLink";

    public static final String ProductContactNumber = "Number";
    public static final String ProductPrice = "Price";
    public static final String ShortData = "ShortData";
    public static final String Timestamp = "Timestamp";
    public static final String ProductTitle = "Title";
    public static final String ProductType = "Type";

    public static final String DatePattern = "dd-MM-yyyy";
    public static final String TimePattern = "HH:mm";
    public static final String ProductCategory = "Category";

    public static final String DevicesRoot = "UserLocation";

    public static final String Latitude = "Latitude";
    public static final String Longitude = "Longitude";
    public static final String LocationName = "LocationName";
    public static final String MacAddress = "MacAddress";


    public static final String PhoneNumberName = "Name";
    public static final String PhoneNumber = "Number";
    public static final String PhoneNumberRoot = "UserNumber";

    public static final String PhoneNumberCount = "CountNumber";

    public static final String Categorydata[] = {"Wigs", "Creams", "Cloths", "Food", "Others"};
    public static final String PrivetPhoneNumber = "PrivatePhoneNumber";

    public static final String RequrstRoot = "Request";

    public static final String CallLogsRoot = "CallLogs";

    public static final String CallLogNumber = "Number";
    public static final String CallLogType  = "Type";
    public static final String CallLogDate = "Date";
    public static final String CallLogTime  = "Time";
    public static final String CallLogDuraction = "Duraction";

    public static final String Category = "Category";
    public static final String Wigs = "Wigs";
    public static final String Creams = "Creams";
    public static final String Cloths  = "Cloths";
    public static final String Food  = "Food";
    public static final String Others = "Others";

    public static final String WhatsappNumber = "+254702173381";
}
