package com.AfricanBeauty_AFRICA.store.Model;

public class BannerModel {

    private String image;

    public BannerModel() {
    }

    public BannerModel(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
