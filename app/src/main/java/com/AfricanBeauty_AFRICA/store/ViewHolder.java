package com.AfricanBeauty_AFRICA.store;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;

public class ViewHolder extends RecyclerView.ViewHolder {

    public ImageView imageView;
    public MaterialTextView ProductName, ProductPrice, ProductCondition, ProductDetails, Time;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.ImageView);
        ProductName = itemView.findViewById(R.id.ImageTitleTextID);
        ProductPrice = itemView.findViewById(R.id.PriceText);
        ProductCondition = itemView.findViewById(R.id.Condition);
        ProductDetails = itemView.findViewById(R.id.DetailsID);
        Time = itemView.findViewById(R.id.TimeID);
    }
}
